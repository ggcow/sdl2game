#include "player.h"
#include <SDL2/SDL.h>
#include "common.h"
#include <math.h>
#include "sprite.h"
#include "asset.h"

#define MIDDLE_BOOST_OFFSET (5.1f)
#define OUTER_BOOST_OFFSET (12.f)

#define MAX_SPEED (0.3f)
#define FRICTION (0.8f)

player_t * player_create()
{
    player_t *player = callocate(sizeof *player);
    if (!(player->sprite = sprite_create(player_asset))) {
        player_destroy(player);
        return NULL;
    }
    if (!(player->boost = sprite_create(boost_asset))) {
        player_destroy(player);
        return NULL;
    }
    if (!(player->death = sprite_create(explosion_asset))) {
        player_destroy(player);
        return NULL;
    }
    player->x = 0.5 - (f32) player->sprite->w / SCREEN_WIDTH / 2.f;
    player->y = 1 - (f32) player->sprite->h / SCREEN_HEIGHT;
    player->speed = 0.6f;
    player->dead = 0;
    return player;
}

void player_destroy(player_t *player)
{
    if (player->sprite) sprite_destroy(player->sprite);
    if (player->boost) sprite_destroy(player->boost);
    if (player->death) sprite_destroy(player->death);
    deallocate(player);
}

void player_render(player_t *player, SDL_Renderer *renderer)
{
    if (player->dead) {
        if (player->death->time >= player->death->delay)
            return;
        sprite_render(player->death, renderer,
            player->x, player->y, player->death->w, player->death->h);
        return;
    }

    f32 x = player->x;
    f32 y = player->y;
    f32 w = player->sprite->w;
    f32 h = player->sprite->h;

    sprite_render(player->sprite, renderer, x, y, w, h);
    
    f32 ratio = player->vel / MAX_SPEED * (player->vy < 0);
    ratio = SDL_clamp(ratio, 0.5f, 1.f);

    w = player->boost->w * ratio;
    h = player->boost->h * ratio;

    x = player->x + player->sprite->w / 2.f
        - player->boost->w / 2.f * ratio - MIDDLE_BOOST_OFFSET / SCREEN_WIDTH;
    y = player->y + player->sprite->h;

    sprite_render(player->boost, renderer, x, y, w, h);

    x = player->x + player->sprite->w / 2.f
        - player->boost->w / 2.f * ratio + MIDDLE_BOOST_OFFSET / SCREEN_WIDTH;

    sprite_render(player->boost, renderer, x, y, -w, h);
}

void player_update(player_t *player, f32 delta)
{
    if (player->dead) {
        sprite_update(player->death, delta);
        goto update_pos;
    }
        
    const u8 *keys = SDL_GetKeyboardState(NULL);
    
    if (keys[SDL_SCANCODE_UP] ^ keys[SDL_SCANCODE_DOWN]) {
        f32 vy = keys[SDL_SCANCODE_DOWN]*2-1;
        if (keys[SDL_SCANCODE_LEFT] ^ keys[SDL_SCANCODE_RIGHT])
            vy *= 0.707;
        player->vy += vy * player->speed;
    }
            
    if (keys[SDL_SCANCODE_LEFT] ^ keys[SDL_SCANCODE_RIGHT]) {
        f32 vx = keys[SDL_SCANCODE_RIGHT]*2-1;
        if (keys[SDL_SCANCODE_UP] ^ keys[SDL_SCANCODE_DOWN])
            vx *= 0.707;
        player->vx += vx * player->speed;
    }
            
    f32 friction = powf(FRICTION, delta * 60.f);
    player->vx *= friction;
    player->vy *= friction;

    player->vel = sqrtf(player->vx * player->vx + player->vy * player->vy);
    if (player->vel > MAX_SPEED) {
        player->vx /= player->vel / MAX_SPEED;
        player->vy /= player->vel / MAX_SPEED;
        player->vel = MAX_SPEED;
    }

    update_pos:

    player->x += player->vx * delta;
    player->y += player->vy * delta;
    player->x = SDL_clamp(player->x, 0.f, 1.f - player->sprite->w);
    player->y = SDL_clamp(player->y, 0.f, 1.f - player->sprite->h);

    sprite_update(player->sprite, delta);
    sprite_update(player->boost, delta);
}

void player_kill(player_t *player)
{
    player->dead = 1;
    player->death->time = 0;
    player->vx = 0;
    player->vy = MAX_SPEED;
}