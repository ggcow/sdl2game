#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "asset.h"
#include "common.h"

asset_t background_asset;
asset_t player_asset;
asset_t missile_asset;
asset_t explosion_asset;
asset_t boost_asset;

static asset_t asset_load(SDL_Renderer *renderer, const char *path, f32 delay, int num_frames)
{
    asset_t asset;
    if (!(asset.tex = IMG_LoadTexture(renderer, path))) {
        log_error("Failed to load texture: %s", IMG_GetError());
        return (asset_t) {};
    }
    asset.delay = delay;
    asset.num_frames = num_frames;
    return asset;
}

static void asset_free(asset_t asset)
{
    SDL_DestroyTexture(asset.tex);
}

void assets_load(SDL_Renderer *renderer)
{
    background_asset = asset_load(renderer, ROOT"/assets/space.png", 1000, 1);
    player_asset = asset_load(renderer, ROOT"/assets/ship/ship_1.png", 0, 1);
    missile_asset = asset_load(renderer, ROOT"/assets/missile.png", 0.3, 2);
    explosion_asset = asset_load(renderer, ROOT"/assets/explosion.png", 600, 7);
    boost_asset = asset_load(renderer, ROOT"/assets/ship/turbo_blue.png", 0.3, 2);
}

void assets_free()
{
    asset_free(background_asset);
    asset_free(player_asset);
    asset_free(missile_asset);
    asset_free(explosion_asset);
    asset_free(boost_asset);
}