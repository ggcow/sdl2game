#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "sprite.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "common.h"

typedef struct background_t {
    SDL_Texture *tex;
    f32 time;
    f32 delay;
} background_t;

background_t * background_create();
void background_destroy(background_t *background);

void background_render(background_t *background, SDL_Renderer *renderer, f32 delta);

#endif