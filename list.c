#include "list.h"
#include "common.h"


void list_remove(list_t *list, void *data)
{
    list_node_t *node = list->head;
    list_node_t *prev = NULL;
    while (node) {
        if (node->data == data) {
            if (prev)
                prev->next = node->next;
            else
                list->head = node->next;
            deallocate(node);
            list->size--;
            return;
        }
        prev = node;
        node = node->next;
    }
}

void* list_pop(list_t *list)
{
    if (!list->head)
        return NULL;
    list_node_t *node = list->head;
    list->head = node->next;
    list->size--;
    void *data = node->data;
    deallocate(node);
    return data;
}

void* list_pop_last(list_t *list)
{
    if (!list->head)
        return NULL;
    list_node_t *node = list->head;
    list_node_t *prev = NULL;
    while (node->next) {
        prev = node;
        node = node->next;
    }
    if (prev)
        prev->next = NULL;
    else
        list->head = NULL;
    list->size--;
    void *data = node->data;
    deallocate(node);
    return data;
}

void list_add_last(list_t *list, void *data)
{
    list_node_t *node = callocate(sizeof *node);
    node->data = data;
    if (!list->head) {
        list->head = node;
        list->size++;
        return;
    }
    list_node_t *last = list->head;
    while (last->next)
        last = last->next;
    last->next = node;
    list->size++;
}

void list_add(list_t *list, void *data)
{
    list_node_t *node = callocate(sizeof *node);
    node->data = data;
    node->next = list->head;
    list->head = node;
    list->size++;
}