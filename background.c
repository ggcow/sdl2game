#include "background.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "common.h"
#include "asset.h"

background_t * background_create()
{
    background_t *background = callocate(sizeof *background);
    background->tex = background_asset.tex;
    background->delay = background_asset.delay;
    background->time = 0;
    return background;
}
void background_destroy(background_t *background)
{
    deallocate(background);
}

void background_render(background_t *background, SDL_Renderer *renderer, f32 delta)
{
    background->time += delta;
    background->time = fmod(background->time, background->delay);
    int y = background->time * SCREEN_HEIGHT / background->delay;
    SDL_Rect src = {
        .x = 0,
        .y = 0,
        .w = SCREEN_WIDTH,
        .h = SCREEN_HEIGHT - y
    };
    SDL_Rect dest = {
        .x = 0,
        .y = y,
        .w = SCREEN_WIDTH,
        .h = SCREEN_HEIGHT - y
    };
    SDL_RenderCopy(renderer, background->tex, &src, &dest);
    src = (SDL_Rect) {
        .x = 0,
        .y = SCREEN_HEIGHT - y,
        .w = SCREEN_WIDTH,
        .h = y
    };
    dest = (SDL_Rect) {
        .x = 0,
        .y = 0,
        .w = SCREEN_WIDTH,
        .h = y
    };
    SDL_RenderCopy(renderer, background->tex, &src, &dest);
}
