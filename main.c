#include <SDL2/SDL.h>
#include <stdio.h>
#include "log.h"
#include "common.h"
#include "player.h"
#include "sprite.h"
#include "background.h"
#include "missile.h"
#include "list.h"
#include "asset.h"

#define exit(s, ...) { log_error(s, ##__VA_ARGS__); goto end; }


int main(int argc, char* args[]) {
  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  background_t *background = NULL;
  player_t *player = NULL;

  if (SDL_Init(SDL_INIT_VIDEO))
    exit("Failed to initialize sdl2: %s", SDL_GetError())

  if (!IMG_Init(IMG_INIT_PNG))
    exit("Failed to initialize SDL_Image")

  window = SDL_CreateWindow(
      "sdl2",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      SCREEN_WIDTH, SCREEN_HEIGHT,
      SDL_WINDOW_SHOWN
      );
  if (!window)
    exit("Failed to create window: %s", SDL_GetError())

  int renderer_flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC;
  if (!(renderer = SDL_CreateRenderer(window, -1, renderer_flags)))
    exit("Failed to create renderer: %s", SDL_GetError())

  assets_load(renderer);

  if (!(background = background_create()))
    goto end;

  if (!(player = player_create()))
    goto end;

  list_t missiles = {};

  SDL_Event event;
  int running = 1;
  unsigned frames = 0;
  f32 time = SDL_GetTicks64() / 1000.f;
  f32 delta = 0;
  f32 last_time = time;
  while (running) {
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          running = 0;
          break;
        case SDL_KEYDOWN:
          if (event.key.keysym.sym == SDLK_ESCAPE)
            running = 0;
          else if (event.key.keysym.sym == SDLK_p && !event.key.repeat) {
            list_add(&missiles, missile_create(player->x, player->y - 0.05, 0.2));
          }
          break;
      }
    }

    player_update(player, delta);
  
    list_foreach(&missiles, missile_t, missile) {
      if (missile_update(missile, delta)) {
        list_remove(&missiles, missile);
        missile_destroy(missile);
      }
    }

    SDL_RenderClear(renderer);
    background_render(background, renderer, 5-player->vy*2);
    
    list_foreach(&missiles, missile_t, missile) {
      missile_render(missile, renderer);
    }
    player_render(player, renderer);
    SDL_RenderPresent(renderer);

    f32 now = SDL_GetTicks64() / 1000.f;
    delta = now - last_time;
    last_time = now;
    frames++;
    if (now - time > 1.f) {
      info("FPS : %u\n", frames);
      log_command("A");
      frames = 0;
      time = now;
    }
  }

  end:
  list_foreach(&missiles, missile_t, missile) {
    list_remove(&missiles, missile);
    missile_destroy(missile);
  }
  if (player) player_destroy(player);
  if (background) background_destroy(background);
  assets_free();
  if (renderer) SDL_DestroyRenderer(renderer);
  if (window) SDL_DestroyWindow(window);
  SDL_Quit();
  return 0;
}