#ifndef MISSILE_H
#define MISSILE_H

#include "sprite.h"
#include "common.h"

typedef struct missile_t {
    sprite_t *sprite;
    f32 x;
    f32 y;
    f32 vx;
    f32 vy;
} missile_t;

missile_t* missile_create(f32 x, f32 y, f32 speed);
void missile_destroy(missile_t *missile);

void missile_render(missile_t *missile, SDL_Renderer *renderer);
int missile_update(missile_t *missile, f32 delta);

#endif