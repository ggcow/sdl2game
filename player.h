#ifndef PLAYER_H
#define PLAYER_H

#include "sprite.h"
#include "common.h"
#include <SDL2/SDL.h>

typedef struct player_t {
    sprite_t *sprite;
    sprite_t *boost;
    sprite_t *death;
    f32 x, y;
    f32 vx, vy;
    f32 speed;
    f32 vel;
    i8 dead;
} player_t;

player_t * player_create();
void player_destroy(player_t *player);

void player_render(player_t *player, SDL_Renderer *renderer);
void player_update(player_t *player, f32 delta);

void player_kill(player_t *player);

#endif