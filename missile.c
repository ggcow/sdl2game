#include "missile.h"
#include "common.h"
#include "asset.h"

missile_t* missile_create(f32 x, f32 y, f32 speed)
{
    missile_t *missile = callocate(sizeof(missile_t));
    if (!(missile->sprite = sprite_create(missile_asset))) {
        missile_destroy(missile);
        return NULL;
    }
    missile->x = x;
    missile->y = y;
    missile->vx = 0;
    missile->vy = -speed;
    return missile;
}

void missile_destroy(missile_t *missile)
{
    if (missile->sprite) sprite_destroy(missile->sprite);
    deallocate(missile);
}

void missile_render(missile_t *missile, SDL_Renderer *renderer)
{
    sprite_render(missile->sprite, renderer, missile->x, missile->y, missile->sprite->w, missile->sprite->h);
}

int missile_update(missile_t *missile, f32 delta)
{
    missile->x += missile->vx * delta;
    missile->y += missile->vy * delta;
    return missile->x > 1
        || missile->x + missile->sprite->w < 0
        || missile->y > 1
        || missile->y + missile->sprite->h < 0;
}
