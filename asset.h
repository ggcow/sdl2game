#ifndef ASSET_H
#define ASSET_H

#include <SDL2/SDL.h>
#include "common.h"

typedef struct asset_t {
    SDL_Texture *tex;
    f32 delay;
    int num_frames;
} asset_t;

void assets_load(SDL_Renderer *renderer);
void assets_free();

extern asset_t background_asset;
extern asset_t player_asset;
extern asset_t missile_asset;
extern asset_t explosion_asset;
extern asset_t boost_asset;

#endif