#ifndef SPRITE_H
#define SPRITE_H

#include "common.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "asset.h"

typedef struct sprite_t {
    SDL_Texture *tex;
    int num_frames;
    SDL_Rect rect;
    f32 w;
    f32 h;
    f32 time;
    f32 delay;
} sprite_t;

sprite_t * sprite_create(asset_t asset);
void sprite_destroy(sprite_t *sprite);

void sprite_update(sprite_t *sprite, f32 delta);
void sprite_render(sprite_t *sprite, SDL_Renderer *renderer, f32 x, f32 y, f32 w, f32 h);

#endif