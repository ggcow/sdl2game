#include "sprite.h"
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "common.h"
#include "asset.h"

#include <math.h>

sprite_t * sprite_create(asset_t asset)
{
    sprite_t *sprite = callocate(sizeof *sprite);
    sprite->tex = asset.tex;
    int w, h;
    SDL_QueryTexture(sprite->tex, NULL, NULL, &w, &h);
    sprite->h = (f32) h / SCREEN_HEIGHT;
    sprite->w = (f32) w / asset.num_frames / SCREEN_WIDTH;
    sprite->num_frames = asset.num_frames;
    sprite->delay = asset.delay;
    sprite->time = 0; 

    return sprite;
}

void sprite_destroy(sprite_t *sprite)
{
    deallocate(sprite);
}

void sprite_update(sprite_t *sprite, f32 delta)
{
    if (sprite->num_frames == 1)
        return;
    sprite->time += delta;
}


void sprite_render(sprite_t *sprite, SDL_Renderer *renderer, f32 x, f32 y, f32 w, f32 h)
{
    SDL_RendererFlip flip = SDL_FLIP_NONE;
    if (w < 0) {
        w = -w;
        flip |= SDL_FLIP_HORIZONTAL;
    }
    if (h < 0) {
        h = -h;
        flip |= SDL_FLIP_VERTICAL;
    }
    

    SDL_Rect dest = {
        .x = roundf(x * SCREEN_WIDTH),
        .y = roundf(y * SCREEN_HEIGHT),
        .w = roundf(w * SCREEN_WIDTH),
        .h = roundf(h * SCREEN_HEIGHT)
    };

    if (sprite->num_frames == 1) {
        SDL_RenderCopy(renderer, sprite->tex, NULL, &dest);
        return;
    }

    sprite->time = fmod(sprite->time, sprite->delay);
    int index = sprite->num_frames * sprite->time / sprite->delay;
    SDL_Rect src = {
        .x = roundf(sprite->w * index * SCREEN_WIDTH),
        .y = 0,
        .w = roundf(sprite->w * SCREEN_WIDTH),
        .h = roundf(sprite->h * SCREEN_WIDTH)
    };

    SDL_RenderCopyEx(renderer, sprite->tex, &src, &dest, 0, NULL, flip);
}