#ifndef LIST_H
#define LIST_H


typedef struct list_node_t {
    void *data;
    struct list_node_t *next;
} list_node_t;


typedef struct list_t {
    list_node_t *head;
    int size;
} list_t;

void list_remove(list_t *list, void *data);
void* list_pop(list_t *list);
void* list_pop_last(list_t *list);
void list_add_last(list_t *list, void *data);
void list_add(list_t *list, void *data);


#define list_foreach(list, type, var) \
    for (struct { \
            list_node_t *node; \
            list_node_t *next; \
        } loopy = { (list)->head, (list)->head ? (list)->head->next : NULL }; \
        loopy.node; \
        loopy.node = loopy.next, loopy.next = loopy.next ? loopy.next->next : NULL) \
    for (type* var = (type*)loopy.node->data; var; var = NULL)

#endif